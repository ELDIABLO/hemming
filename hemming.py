from PIL import Image
import numpy as np
import os
import math

# -----------------------------------------------------------
# ЗАДАНИЕ НАЧАЛЬНЫХ УСЛОВИЙ
# -----------------------------------------------------------

Debug = False

# Функция активации
def activation_func(s, T):
    return {
        s <= 0 : 0,
        0 < s <= T : s,
        s >= T : s
    }[True]

# Чтение эталонных изображений
# Матрица эталонных образов
images = []
for file in os.listdir("ciphers"):
    im = Image.open("ciphers/"+file)
    p = abs(np.array(im)/255-1)
    image = []
    for i in range(0, len(p)):
        for j in range (0, len(p[0])):
            image.append(p[i][j][0])
    images.append(image)
images = np.array(images)

# Матрица weights
W = 0.5*images
if Debug:
    print('---------------------------------W---------------------------------------')
    print(len(W))
    print(len(W[0]))
    print(W)

# Параметр для функции активации
T = len(images[0])/2
if Debug:
    print('---------------------------------T---------------------------------------')
    print(T)

# Матрица весов обратных связей
Eps = 1/len(images)
E = []
for i in range(0, len(images)):
    temp=[]
    for j in range(0, len(images)):
        if i==j:
            temp.append(1)
        else:
            temp.append(-1*Eps)
    E.append(temp)
E = np.array(E)
if Debug:
    print('---------------------------------E---------------------------------------')
    print(len(E))
    print(len(E[0]))
    print(E)

# Разница выхода
Emax = 0.1

# -----------------------------------------------------------
# ПРОГОН
# -----------------------------------------------------------

# Загрузка изображения (подаем на вход)
im = Image.open("test.bmp")
p = abs(np.array(im)/255-1)
image = []
for i in range(0, len(p)):
    for j in range (0, len(p[0])):
        image.append(p[i][j][0])
image = np.array([image])
if Debug:
    print('---------------------------------image---------------------------------------')
    print(len(image))
    print(len(image[0]))
    print(image)

# 1 Layer
S1 = np.dot(W, image.transpose())+T
if Debug:
    print('---------------------------------S1---------------------------------------')
    print(len(S1))
    print(len(S1[0]))
    print(S1)
Y1 = []
for i in range(0, len(S1)):
    temp = []
    for j in range (0, len(S1[0])):
        temp.append(activation_func(S1[i][j], T))
    Y1.append(temp)
if Debug:
    print('---------------------------------Y1---------------------------------------')
    print(len(Y1))
    print(len(Y1[0]))
    print(Y1)

# 2 Layer
Y2_prev = np.array(Y1)
while True:
    S2 = np.dot(E, Y2_prev)
    Y2 = []
    for i in range(0, len(S2)):
        temp = []
        for j in range(0, len(S2[0])):
            temp.append(activation_func(S2[i][j], T))
        Y2.append(temp)
    Value = 0
    for i in range(0, len(Y2)):
        for j in range(0, len(Y2[0])):
            Value = Value + (Y2[i][j]-Y2_prev[i][j])*(Y2[i][j]-Y2_prev[i][j])
    Value=math.sqrt(Value)
    print(Value)
    if Value<=Emax:
        break
    Y2_prev = Y2

print(Y2)